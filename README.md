### Installing and Starting the application
```bash
$ npm install
$ npm start
```

The client can be started with `npm start`.

The site can be viewed at `http://localhost:3000`

### Comments

I enjoyed the challenge and I feel like I made good progress. Given more time I would have resoled the TODOs, polished the network retry logic, added tests, and added typescript.

I wanted to mention that I noticed a couple places where the spec and the server implementation differ:
- The API spec uses `/space` but the implementation uses `/spaces`
- The result for `/spaces` was quite different than documented for `/space`
