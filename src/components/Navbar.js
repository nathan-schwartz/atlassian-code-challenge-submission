import React from 'react';
import { drawerWidth } from '../config';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

// TODO: Use elipsis instead of overflow:hidden
const useStyles = makeStyles({
  drawer: {
    width: drawerWidth,
    overflow: 'hidden'
  },
  drawerPaper: {
    width: drawerWidth,
    overflow: 'hidden'
  },
});

function Navbar({ navItems, activeSpaceId, updateActiveSpace }) {
  const classes = useStyles();
  return (
    <Drawer
      variant="permanent"
      anchor="left"
      className={classes.drawer}
      classes={{
        paper: classes.drawerPaper
      }}>
      <h3 style={{ paddingLeft: 20 }}>Spaces</h3>
      <List>
        <Divider/>
        {navItems.map(({ id, title }) => (
          <div key={"item-wrapper-" + id}>
            <ListItem button selected={activeSpaceId === id} onClick={() => updateActiveSpace({ title, id })}>
              <ListItemText primary={title} />
            </ListItem>
            <Divider/>
          </div>
        ))}
        </List>
      </Drawer>
  );
}

export default Navbar;
