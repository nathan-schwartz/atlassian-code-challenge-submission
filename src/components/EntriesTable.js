import React, { useState, useEffect } from 'react';
import EnhancedTable from './Table';
import { fetchAuthorName } from '../helpers';

const columnConfig = [
  { id: 'title', label: "Title", numeric: false, disablePadding: true },
  { id: 'summary', label: "Summary", numeric: false, disablePadding: true },
  { id: 'updatedAt', label: "Updated At", numeric: false, disablePadding: true, formatter: (row) => new Date(row.updatedAt).toLocaleDateString() },
  { id: 'createdBy', label: "Created By", numeric: false, disablePadding: true },
  { id: 'updatedBy', label: "Updated By", numeric: false, disablePadding: true },
];

function SpaceDetails({ spaceData }) {
  const [rows, setRows] = useState([]);

  // TODO: Maybe extract name fetching logic to be shared with AssetsTable
  // TODO: Handle network errors if the fetch fails
  useEffect(() => {
    const assetIds = Object.keys(spaceData.Assets)
      .filter(k => k !== 'description');

    // Aggregate and dedupe userIds so we only fetch each once.
    // Could be improved in the future by implementing caching
    const userIds = Object.keys(assetIds.reduce((acc, id) => {
      const cur = spaceData.Assets[id];
      acc[cur.sys.updatedBy] = true;
      acc[cur.sys.createdBy] = true;
      return acc;
    }, {}));

    // Will store:name associations
    const userIdToNameMap = {};

    Promise.all(userIds.map(id =>
      fetchAuthorName(id).then((name) => {
        userIdToNameMap[id] = name;
      })
    )).then(() => {
      const populatedRows = Object.keys(spaceData.Entries).map(id => {
        const cur = spaceData.Entries[id]
        return {
          title: cur.fields.title,
          summary: cur.fields.summary,
          updatedAt: Number(new Date(cur.sys.updatedAt)),
          updatedBy: userIdToNameMap[cur.sys.updatedBy],
          createdBy: userIdToNameMap[cur.sys.createdBy],
          id: cur.sys.id,
        }
      });
      setRows(populatedRows);
    });
  }, [spaceData.sys.id]);

  return <EnhancedTable rows={rows} columnConfig={columnConfig}/>
}

export default SpaceDetails;

