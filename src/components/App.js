import React, { useState, useEffect } from 'react';
import Navbar from './Navbar';
import { fetchSpaces } from '../helpers';
import SpaceDetails from './SpaceDetails';
import promiseRetry from 'promise-retry';

// TODO: Consistent use of useStyles vs styles objects throughout the components
// TODO: Refetch data periodically

function App() {
  const [spaces, setSpaces] = useState([]);
  const [navItems, setNavItems] = useState([]);
  // Get initial state from URL. TODO: Currently doesn't tolerate any other url structure
  const [activeSpaceId, setActiveSpaceId] = useState(window.location.pathname === '/' ? null : window.location.pathname.slice(1));

  // Ensure Browser history and url are updated (if supported).
  const updateActiveSpace = ({ title, id }) => {
    if (window.history && window.history.pushState && `/${id}` !== window.location.pathname) {
      window.history.pushState({}, title || null, `/${id}`);
    }
    setActiveSpaceId(id);
  }

  // Right now this only runs once, on mount.
  // Rudimentary API retries are implemented.
  useEffect(() => {
    console.log('fetchinng');
    promiseRetry((retry, count) => {
      console.log('retrying');
      return fetchSpaces()
        .then(data => {
          console.log('data', data);

          // Store response in memory
          setSpaces(data)

          const newNavItems = Object.keys(data).filter(k => k !== 'description')

          // Respect initial URL value
          updateActiveSpace({ id: activeSpaceId || newNavItems[0] || null });
          setNavItems(newNavItems.map(k => ({ id: k, title: data[k].fields.title })));
        })
        .catch(err => {
          console.error(`/spaces request failed (${count} times)`, err);
          retry(err);
        })
    }, { maxTimeout: 10 * 1000, retries: 1000 });
  }, []);

  return (
    <div className="App">
      <Navbar navItems={navItems} activeSpaceId={activeSpaceId} updateActiveSpace={updateActiveSpace} />
      {spaces[activeSpaceId] ? <SpaceDetails spaceData={spaces[activeSpaceId]} /> : <div> Not available </div>}
    </div>
  );
}

export default App;
