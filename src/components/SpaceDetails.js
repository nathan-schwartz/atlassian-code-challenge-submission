import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from '../config';
import { fetchAuthorName } from '../helpers';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import EntriesTable from './EntriesTable';
import AssetsTable from './AssetsTable';

const useStyles = makeStyles({
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  textField: {
    width: 300,
    marginTop: 100,
    marginLeft: 30,
    marginBottom: 30,
  },
  tabs: {
    marginLeft: 30,
    marginRight: 30,
  }
});

function SpaceDetails({ spaceData }) {
  const classes = useStyles();
  const [currentTab, setCurrentTab] = useState(0);
  const [authorName, setAuthorName] = useState(null);

  // Fetch author name
  // TODO: Handle network errors
  useEffect(() => {
    fetchAuthorName(spaceData.sys.createdBy)
      .then((name) => setAuthorName(name))
      .catch(err => {
        console.error(`/users/${spaceData.sys.createdBy} Request failed`, err);
        setAuthorName(null);
      });
  }, [spaceData.sys.createdBy]);

  return (
    <div style={{ width: `calc(100% - ${drawerWidth}px)`, marginLeft: drawerWidth }}>
      <AppBar className={classes.appBar} >
        <Toolbar>
          <h2 style={{ display: 'inline' }}> {spaceData.fields.title} </h2>
          {authorName && <h5 style={{ display: 'inline', marginLeft: 20 }}> created by {authorName} </h5>}
        </Toolbar>
      </AppBar>

      <p style={{ marginTop: 100, marginLeft: 30, marginBottom: 30 }}>{spaceData.fields.description}</p>

      <Tabs
        value={currentTab}
        className={classes.tabs}
        onChange={(e, val) => setCurrentTab(val)}
        indicatorColor="primary"
        textColor="primary"
        variant="fullWidth">
          <Tab label="Entries" />
          <Tab label="Assets" />
      </Tabs>
      {currentTab === 0 && <EntriesTable spaceData={spaceData} />}
      {currentTab === 1 && <AssetsTable spaceData={spaceData} />}
    </div>
  )
}

export default SpaceDetails;
