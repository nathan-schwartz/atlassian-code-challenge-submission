// A file to house common configuration variables

// Expected not to end with a slash.
export const API_URL = 'http://localhost:3001';

// Not a good place for this but better than repeatign it everywhere
export const drawerWidth = 250;
