import { API_URL } from './config';

export const fetchAuthor = (id) =>
    fetch(`${API_URL}/users/${id}`)
      .then((res) => {
        if (res.status >= 400) {
          throw new Error(`Requeest failed with status=${res.status}`);
        }
        return res.json();
      })

export const fetchAuthorName = (id) =>
  fetchAuthor(id)
      .then((data) => {
        if (data && data.fields && data.fields.name) {
          return data.fields.name;
        } else {
          return null;
        }
      });

export const fetchSpaces = () =>
    fetch(`${API_URL}/spaces`)
      .then((res) => {
        if (res.status >= 400) {
          throw new Error(`Requeest failed with status=${res.status}`);
        }
        return res.json();
      })
